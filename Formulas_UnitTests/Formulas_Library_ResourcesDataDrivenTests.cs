﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Formulas_Service;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;

namespace Formulas_UnitTests
{
    [TestClass]
    public class Formulas_Library_ResourcesDataDrivenTests
    {
        XDocument doc;
        List<Variables> list;

        [TestInitialize]
        public void TestInitialize()
        {
            doc = XDocument.Parse(Formulas_UnitTests.Properties.Resources.VariablesFromDB);
            list = new List<Variables>();
            foreach (XElement item in doc.Root.Elements("row"))
                list.Add(new Variables
                {
                    varName = item.Attribute("name").Value,
                    varExpressionRPN = item.Attribute("rpn").Value
                });
        }

        [TestMethod]
        public void Validate_fromXML()
        {
            foreach (Variables variable in list)
            {
                bool expected = Convert.ToBoolean(doc.Root.Elements()
                    .Where(r => r.Attribute("name").Value == variable.varName)
                    .Select(r => r.Attribute("possible").Value)
                    .First());
                bool actual = (new Formula
                    {
                        Name = variable.varName,
                        ExpressionRPN = variable.varExpressionRPN,
                        VariablesFromDB = list
                    }).Validate();
                Assert.AreEqual(expected, actual, variable.varName + ":  expected: {0}, actual: {1}", expected.ToString(), actual.ToString());
            }
        }

        [TestMethod]
        public void Calculate_fromXML()
        {
            foreach (Variables variable in list)
            {
                //double? expected = null; if (TestContext.DataRow["value"].ToString() != "") expected = Convert.ToDouble(TestContext.DataRow["value"]);
                double? expected = Convert.ToDouble(doc.Root.Elements()
                    .Where(r => r.Attribute("name").Value == variable.varName)
                    .Select(r => r.Attribute("value").Value)
                    .First());
                double? actual = (new Formula
                    {
                        Name = variable.varName,
                        ExpressionRPN = variable.varExpressionRPN,
                        VariablesFromDB = list
                    }).CalculateValue();
                Assert.AreEqual(expected, actual, variable.varName + ":  expected: {0}, actual: {1}", expected.ToString(), actual.ToString());
            }
        }
    }
}