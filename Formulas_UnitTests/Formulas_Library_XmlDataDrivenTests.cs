﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Formulas_Service;


namespace Formulas_Service.Tests
{
    [TestClass]
    public class Formulas_Library_XmlDataDrivenTests
    {
        public TestContext TestContext { get; set; }

        private string name;
        private string expression;
        private string correct;
        private string rpn;

        [TestInitialize]
        public void TestInitialize()
        {
            name = Convert.ToString(TestContext.DataRow["name"]);
            expression = Convert.ToString(TestContext.DataRow["expression"]);
            correct = Convert.ToString(TestContext.DataRow["correct"]);
            rpn = Convert.ToString(TestContext.DataRow["rpn"]);
        }

        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
            "TestData.xml", "row", Microsoft.VisualStudio.TestTools.UnitTesting.DataAccessMethod.Sequential)]
        [TestMethod]
        public void GetStatus_fromXML()
        {
            string actual = (new Formula { Expression = expression }).GetStatus();
            if (actual.Contains("Ошибка"))
                actual = "Ошибка";
            Assert.AreEqual(correct, actual);
        }

        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
            "TestData.xml", "row", Microsoft.VisualStudio.TestTools.UnitTesting.DataAccessMethod.Sequential)]
        [TestMethod]
        public void GetReversePolishNotation_fromXML()
        {
            string actual = (new Formula { Expression = expression }).GetReversePolishNotation();
            Assert.AreEqual(rpn, actual);
        }

        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
            "TestData.xml", "row", Microsoft.VisualStudio.TestTools.UnitTesting.DataAccessMethod.Sequential)]
        [TestMethod]
        public void MinusSymbol_Is_UnaryOperation_fromXML()
        {
            bool expected = Convert.ToBoolean(TestContext.DataRow["MinusUnary"]);
            int MinusPos;
            bool actual = false;
            if (TestContext.DataRow["MinusPos"].ToString() != "")
            {
                MinusPos = Convert.ToInt32(TestContext.DataRow["MinusPos"]);
                actual = (new Formula { Expression = expression }).MinusSymbol_Is_UnaryOperation(MinusPos);
            }
            Assert.AreEqual(expected, actual);
        }
    }
}