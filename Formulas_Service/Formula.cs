﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Formulas_Service
{
    public class Formula
    {
        /// <summary>
        /// Имя переменной.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Математическое выражение определяющее переменную.
        /// </summary>
        public string Expression { get; set; }
        /// <summary>
        /// Строка содержащая выявленные ошибки в выражении; 
        /// или "correct" в случае их отсутсвия; 
        /// или "inprogress" в случае, если проверка синтаксиса выражения ещё не проводилась. 
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// Выражение в формате обратной польской записи определяющее переменную.
        /// </summary>
        public string ExpressionRPN { get; set; }
        /// <summary>
        /// Численное значение переменной.
        /// </summary>
        public double? Value { get; set; }
        /// <summary>
        /// Список переменных в базе данных на момент создания объекта.
        /// </summary>
        public List<Variables> VariablesFromDB { get; set; }

        public Formula()
        { }

        /// <summary>
        /// Проверяет корректность введённого выражения, возвращает "correct", если выражение корректно или строку с указанием выявленных ошибок в выражении.
        /// </summary>
        /// <param name="_expression">выражение, правая часть строки введённая пользователем (правая от знака равенства '=').</param>
        /// <returns></returns>
        public string GetStatus()
        {
            string result = "";

            if (Expression == "")                           // Если выражение для переменной не введено
                return "Ошибка ввода: пустое выражение.\n"; //

            if (Expression.Contains('='))                                              // Параметр _expression это есть правая часть введённого уравнения,
                return "Ошибка ввода: лишний знак равенства: " + Expression + "\n";    // знак равенства здесь уже не нужен.

            int bracketsBalance = 0;                                                                // Проверяем баланс скобок '(' и ')'
            for (int pos = 0; pos < Expression.Length; pos++)                                       //
            {                                                                                       //
                if (Expression[pos] == '(')                                                         //
                    bracketsBalance++;                                                              //
                if (Expression[pos] == ')')                                                         //
                    bracketsBalance--;                                                              //
                if (bracketsBalance < 0)                                                            // Если закрывающая расположена до открывающей (при этом гипотетически баланс м.б. = 0)
                {                                                                                   //
                    result += "Ошибка ввода: несогласованные скобки: " + Expression + "\n";         //
                    break;                                                                          //
                }                                                                                   //
            }                                                                                       //
            if (bracketsBalance != 0 && !result.Contains("Ошибка ввода: несогласованные скобки"))   //
                result += "Ошибка ввода: несогласованные скобки: " + Expression + "\n";             //

            for (int pos = 0; pos < Expression.Length; pos++)                                                                  // Проверяем "укомплектованность"
                if (("+-*/").IndexOf(Expression[pos]) != -1)                                                                   // бинарных операторов операндами.
                    if (pos == 0)                                                                                              // В начале выражения может стоять только унарный 'минус' или операнд.
                    {                                                                                                          //
                        if (Expression[pos] != '-')                                                                            //
                            result += "Ошибка ввода: выражение начинается с бинарного оператора: " + Expression + "\n";        //
                    }                                                                                                          //
                    else if (pos == Expression.Length - 1)                                                                     // В конце выражения может стоять только операнд.
                        result += "Ошибка ввода: выражение заканчивается бинарным оператором: " + Expression + "\n";           //
                    else if ((IsOperator(Expression[pos - 1]) && Expression[pos - 1] != ')' && Expression[pos] != '-')         // Если    предыдущий символ = оператор, но не ')', а текущий не '-'
                           || IsOperator(Expression[pos + 1]) && Expression[pos + 1] != '(' && Expression[pos + 1] != '-')     // или если следующий символ = оператор, но не '(' и не '-'
                        result += "Ошибка ввода: бинарные операторы расположены подряд: " + Expression + "\n";                 //

            if (result == "")          // Если ошибок не выявлено, 
                result = "correct";    // Возвращаем "correct"
            return result;
        }

        /// <summary>
        /// Возвращает выражение в формате обратной польской записи (ОПЗ).
        /// </summary>
        /// <param name="_expression">выражение, правая часть строки введённая пользователем (правая от знака равенства '=').</param>
        /// <returns></returns>
        public string GetReversePolishNotation()
        {
            //3+4*2/(1-5)
            //3 4 2 * 1 5 - / +

            string result = "";
            Stack<char> temp = new Stack<char>(); // вспомогательный стек для сортировки операндов и операторов

            for (int pos = 0; pos < Expression.Length; pos++) // посимвольно по всему выражению
            {
                if (Expression[pos] == '$') // Блок обработки условий, циклов и локальных переменных.
                {                           //
                                            //
                                            //
                }                           //

                if (Char.IsLetterOrDigit(Expression[pos]) || Expression[pos] == ',') // Если операнд,
                {                                                                    //
                    while (!IsOperator(Expression[pos]))                             // то посимвольно 
                    {                                                                // помещаем операнд в ОПЗ.
                        result += Expression[pos];                                   // 
                        pos++;                                                       // 
                        if (pos == Expression.Length)                                // Если символы кончились,
                            break;                                                   // выходим из цикла.
                    }                                                                //
                    result += " ";                                                   // Отделяем операнд пробелом от следующего элемента в ОПЗ.
                }

                if (pos >= Expression.Length) // Если это был последний символ выражения,
                    break;                    // заканчиваем цикл.

                if (IsOperator(Expression[pos]))    // Если оператор, то смотрим какой именно:
                    if (Expression[pos] == '(')         // Если '(', 
                        temp.Push(Expression[pos]);     // то помещаем её в стек.
                    else if (Expression[pos] == ')')    // Если ')', 
                    {                                   // 
                        while (temp.Peek() != '(')      // то все операторы до '(', 
                            result += temp.Pop() + " "; // выталкиваем из стека,
                        temp.Pop();                     // а '(' - удаляем из стека.
                    }
                    else if (Expression[pos] == '-' && MinusSymbol_Is_UnaryOperation(pos)) // Провереям знак 'минус' на унарность:
                    {                                                                      // Если 'минус' это унарная операция, 
                        result += "0 ";                                                    // то переводим выражение "-A" (воспринимая как "0-А") 
                        temp.Push('-');                                                    // в ОПЗ в виде:       "0 A -".
                    }                                                                      //
                    else
                    {                                                       // Если '+', или '-', или '*', или '/',
                        if (temp.Count > 0)                                 // то если в стеке операторы есть,
                            while (GetOperatorPriority(temp.Peek()) >=      // то сверху вниз все операторы с приоритетом больше
                                   GetOperatorPriority(Expression[pos]))    // приоритета текущего (рассматриваемого) оператора
                            {                                               // 
                                result += temp.Pop() + " ";                 // выталкиваем из стека в строку-результат.
                                if (temp.Count == 0)                        //
                                    break;                                  //
                            }                                               //
                        temp.Push(Expression[pos]);                         // А затем помещаем в стек текущий оператор.
                    }                                                       //
            }
                                              // Выражение закончилось.
            while (temp.Count > 0)            // Все операторы в стеке
                result += temp.Pop() + " ";   // выталкиваем из стека в строку-результат.

            result = result.Trim(); // Удаляем последний "лишний" пробел.
            return result;
        }

        /// <summary>
        /// Показывает, является ли знак 'минус' унарной операцией в составе выражения.
        /// </summary>
        /// <param name="expression">выражение, правая часть строки введённая пользователем (правая от знака равенства '=').</param>
        /// <param name="positionOfMinusSymbol">позиция символа '-' в строке начиная с ноля.</param>
        /// <returns></returns>
        public bool MinusSymbol_Is_UnaryOperation(int positionOfMinusSymbol)
        {
            if (Expression[positionOfMinusSymbol] != '-')
                return false;

            if (positionOfMinusSymbol == 0)                             // Если 'минус' это первый символ,
                return true;                                            // то 'минус' это унарная операция.
            else if (IsOperator(Expression[positionOfMinusSymbol - 1]))     // Если предыдущий символ оператор, 
                return true;                                                // то 'минус' это унарная операция.
            else                                                        // Если же перед знаком 'минус' расположен операнд,
                return false;                                           // то 'минус' это бинарная операция.
        }

        /// <summary>
        /// Показывает, является ли символ оператором: ' ( ', или ' ) ', или ' + ', или ' - ', или ' * ', или ' / '.
        /// </summary>
        /// <param name="_c">проверяемый символ</param>
        /// <returns></returns>
        private bool IsOperator(char _symbol)
        {
            if ("()+-*/".IndexOf(_symbol) != -1)  // Если символ входит в указанную строку,
                return true;                      // то он оператор.
            else
                return false;
        }

        /// <summary>
        /// Возвращает приоритет оператора от 0 до 5 исходя из следующего ряда (в порядке возрастания приоритета): " ( ) + - * / "
        /// </summary>
        /// <param name="_c">Символ оператора</param>
        /// <returns></returns>
        private int GetOperatorPriority(char _operator)
        {                                       // Операторы в строке "()+-*/" выстроены по порядку возрастания их приоритета.
            return "()+-*/".IndexOf(_operator); // Возвращает номер позиции символа в строке "()+-*/" начиная с ноля. Этот номер и сеть приоритет.
        }

        /// <summary>
        /// Возвращает значение переменной или null в случае деления на ноль.
        /// </summary>
        /// <returns></returns>
        public double? CalculateValue()
        {
            List<string> input = new List<string>(ExpressionRPN.Split(' ')); // Формируем лист аргументов и операций в формате ОПЗ.
            Stack<double> result = new Stack<double>(); // Стек для вывода (подсчёта).
            for (int i = 0; i < input.Count; i++) // Перебираем входной лист слева направо:
            {
                if (Char.IsLetter(input[i][0]))                             // Если переменная
                    foreach (Variables variable in VariablesFromDB)         // то перебираем переменные в базе и
                        if (variable.varName.Equals(input[i]))              // находим нашу текущую по имени.
                        {                                                   // 
                            Formula formula = new Formula                   // |
                            {                                               // |
                                Name = variable.varName,                    // |
                                ExpressionRPN = variable.varExpressionRPN,  // | Формируем объект.
                                VariablesFromDB = this.VariablesFromDB      // |
                            };                                              // |
                            if (formula.Value == null)                      // Если значение ранее не вычислялось
                                formula.Value = formula.CalculateValue();   // вычисляем.
                            input[i] = formula.Value.ToString();            // Заменяем переменную в листе выражения на её значение.
                            break;
                        }
                if (Char.IsDigit(input[i][0]) || input[i][0] == ',') // Если численное значение
                    result.Push(Double.Parse(input[i]));             // помещаем в стек для вывода (подсчёта).
                else if (IsOperator(input[i][0]))         // Если оператор,
                {                                         // то из стека для вывода (подсчёта) сначала 
                    double b = result.Pop();              // извлекаем второй операнд (правый от оператора в обычной (инфиксной) нотации),
                    double a = result.Pop();              // а затем первый операнд    (левый от оператора в обычной (инфиксной) нотации).
                    switch (input[i][0])                        // 
                    {                                           // Смотрим какой конкретно оператор,
                        case '-':                               // 
                            result.Push(a - b);                 // вычисляем и помещаем значение в стек для вывода (подсчёта).
                            break;                              //
                        case '+':                               //
                            result.Push(a + b);                 //
                            break;                              //
                        case '*':                               //
                            result.Push(a * b);                 //
                            break;                              //
                        case '/':                               //
                            if (b != 0)                         //
                                result.Push(a / b);             //
                            else                                                // Проверка деления на ноль.
                            {                                                   //
                                if (Status == "correct")                        //
                                    Status = "";                                //
                                Status += "При подсчёте переменной \"" + Name + //
                                          "\" обнаружено деление на ноль.\n";   //
                                return null;                                    //
                            }
                            break;
                    }
                }
            }
            return result.Peek();
        }

        /// <summary>
        /// Проверяет, достаточно ли в базе данных информации для подсчёта значения выражения (все ли входящие переменные можно рассчитать).
        /// </summary>
        /// <param name="_DependencesChain">
        /// "Цепочка зависимостей"
        /// При первом вызове указывать не нужно.
        /// Служебный, рекурсивно накопительный список переменных, в формулах которых содержится переменная подлежащая текущей проверке.
        /// Служит для выявления круговой зависимости переменных.
        /// </param>
        /// <returns></returns>
        public bool Validate(List<string> _DependencesChain = null)
        {
            if (ExpressionRPN == "" || ExpressionRPN == null) // Если выражение пустое, 
                return false;                                 // то его рассчитать нет возможности.

            if (_DependencesChain == null)                  // Если список пуст
                _DependencesChain = new List<string>();     // создаём его.
            _DependencesChain.Add(Name);                    // Добавляем текущую переменную в цепочку зависимости.

            List<string> input = new List<string>(ExpressionRPN.Split(' ')); // Формируем лист аргументов и операций в формате ОПЗ.
            for (int i = 0; i < input.Count; i++)     // Перебираем входной лист слева направо
            {                                         //
                if (Char.IsLetter(input[i][0]))       // рассматривая только переменные-аргументы входящие в выражение.
                {
                    bool flag_VariableWasFounded = false;                       //
                    foreach (Variables variable in VariablesFromDB)             // Перебираем переменные в базе,
                        if (variable.varName.Equals(input[i]))                  // находим нашу текущую переменную-аргумент по имени.
                        {                                                       //
                            Formula formula = new Formula                       //
                                {                                               //
                                    Name = variable.varName,                    //
                                    ExpressionRPN = variable.varExpressionRPN,  //
                                    VariablesFromDB = this.VariablesFromDB      //
                                };                                              //
                            flag_VariableWasFounded = true;                     //
                            foreach (string item in _DependencesChain)                  // Если в цепочке зависимостей, которая сформирована рекурсивно
                                if (item == input[i])                                   // содержится текущая переменная, то круг замкнулся.
                                    return false;                                       // При круговой зависимости рассчитать нет возможности.
                            if (!formula.Validate(new List<string>(_DependencesChain))) // Рекурсивно проверяем возможность подсчёта.
                                return false;                         // если хотя бы один параметр выражения нельзя рассчитать - возвращаем false
                            break;
                        }
                    if (!flag_VariableWasFounded) // Если в базе еще пока нет искомой переменной,
                        return false;             // то рассчитать нет возможности - возвращаем false.
                }
            }
            return true; // Если переменных нет (или все они вернули  true), значит можно посчитать - возвращаем true.
        }
    }
}