﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Formulas_Service
{
    // ПРИМЕЧАНИЕ. Команду "Переименовать" в меню "Рефакторинг" можно использовать для одновременного изменения имени интерфейса "IMainService" в коде и файле конфигурации.
    [ServiceContract]
    public interface IInputService
    {
        [OperationContract]
        string Input(string _input);
    }

    [ServiceContract]
    public interface IOutputService
    {
        [OperationContract]
        string Output(string _variableName);
    }

    [ServiceContract]
    public interface IMonitorService
    {
        [OperationContract]
        string Monitor();
    }
}