﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Formulas_Service
{
    // ПРИМЕЧАНИЕ. Команду "Переименовать" в меню "Рефакторинг" можно использовать для одновременного изменения имени класса "MainService" в коде и файле конфигурации.
    public class MainService : IInputService, IOutputService, IMonitorService
    {
        Formulas_localDBEntities context = new Formulas_localDBEntities();

        public string Input(string _input)
        {
            _input = _input.Replace(" ", "").Replace(".", ","); // Убираем пробелы и заменяем '.' на  ','
            string name;
            string expression;
            if (_input[0] == '$') // Если команда
            {
                string operation = _input.Substring(1, _input.IndexOf(':') - 1); // Операция = всё что между '$' и ':'
                switch (operation)
                {                  //  012345678
                    case "delete": // "$delete:A"
                        name = _input.Substring(_input.IndexOf(':') + 1); // Имя переменной = всё что после ':'
                        return DeleteVariable(name);
                    //                   012345678901
                    case "override": // "$override:A=B+C"
                        if (!_input.Substring(_input.IndexOf(':')).Contains('='))   // Если справа от ':' отсутствует '=',
                            return "Ошибка: Формула не содержит знак равенства.";   // то ошибка.
                        name = _input.Substring(_input.IndexOf(':') + 1,                        // Имя переменной = 
                                                _input.IndexOf('=') - _input.IndexOf(':') - 1); // всё что между ':' и '='.
                        expression = _input.Substring(_input.IndexOf('=') + 1); // Выражение = всё что справа от знака'='.
                        return OverrideVariable(name, expression);
                    default:
                        return "Неверная команда.";
                }
            }
            else
            { // Обычная попытка ввода новой переменной.
                if (!_input.Contains('='))
                    return "Ошибка: Формула не содержит знак равенства.";
                name = _input.Substring(0, _input.IndexOf('=')); // Имя переменной  = всё, что слева от знака '=' 
                expression = _input.Substring(_input.IndexOf('=') + 1); // Выражение = всё что справа от знака'='
                return AddVariable(name, expression);
            }
        }

        /// <summary>
        /// Добавляет переменную в базу данных.
        /// Возвращает сообщение об успешном добавлении или 
        /// о существовании ранее введённой переменной с заданным именем в базе.
        /// </summary>
        /// <param name="_variableName">Имя переменной</param>
        /// <param name="_expression">Выражение определяющее переменную</param>
        /// <returns></returns>
        private string AddVariable(string _variableName, string _expression)
        {
            //if (context.Variables.Count(v => v.Name.Equals(variableName, StringComparison.Ordinal)) != 0)
            foreach (Variables variable in context.Variables)                       // Проверяем: 
                if (variable.varName.Equals(_variableName))                         // не занято ли такое имя переменной
                    return "Переменная \"" + _variableName + "\" уже существует.";  //

            context.Variables.Add(new Variables    // Добавляем новую переменную в БД.
            {                                      //
                varID = Guid.NewGuid(),            //
                varName = _variableName,           //
                varExpression = _expression,       //
                varStatus = "inprogress"           //
            });
            context.SaveChanges(); // Сохраняем изменения в БД.
            return "Переменная добавлена в БД.";
        }

        /// <summary>
        /// Переопределяет переменную в базе данных.
        /// Возвращает сообщение об успехе,
        /// либо об отсутсвии переменной с указанным именем.
        /// </summary>
        /// <param name="_variableName">Имя переменной</param>
        /// <param name="_expression">Выражение определяющее переменную</param>
        /// <returns></returns>
        private string OverrideVariable(string _variableName, string _expression)
        {
            bool flagVariableWasFounded = false;
            foreach (Variables variable in context.Variables) // Ищем переменную в БД.
                if (variable.varName.Equals(_variableName))   // по имени
                {                                             // 
                    flagVariableWasFounded = true;            //
                    variable.varExpression = _expression;     // Присваиваем переменной новое выражение.
                    variable.varExpressionRPN = "";           // Значения остальных свойств переменной приводим 
                    variable.varStatus = "inprogress";        // к значениям как-бы вновь введённой переменной.
                    variable.varValue = null;                 //
                    break;
                }
            if (flagVariableWasFounded)                                         // Если переменная с заданным 
            {                                                                   // именем была найдена,
                context.SaveChanges();                                          // сохраняем изменения в БД.
                Switch_DependentVariables_InTo_InProgressStatus(_variableName);
                return "Переменная \"" + _variableName + "\" переопределена.";
            }
            else
                return "Переменной \"" + _variableName + "\" не существует.";
        }

        /// <summary>
        /// Удаляет переменную из базы данных.
        /// Возвращает сообщение об успехе,
        /// либо об отсутсвии переменной с указанным именем.
        /// </summary>
        /// <param name="_variableName">Имя переменной</param>
        /// <returns></returns>
        private string DeleteVariable(string _variableName)
        {
            bool flagVariableWasFounded = false;
            foreach (Variables variable in context.Variables) // Ищем переменную в БД.
                if (variable.varName.Equals(_variableName))   // по имени
                {                                             //
                    flagVariableWasFounded = true;            // фиксируем факт нахождения переменной
                    context.Variables.Remove(variable);       // и удаляем переменную.
                    break;
                }
            if (flagVariableWasFounded)                                  // Если переменная с заданным 
            {                                                            // именем была найдена,
                context.SaveChanges();                                   // cохраняем изменения в БД.
                Switch_DependentVariables_InTo_InProgressStatus(_variableName);
                return "Переменная \"" + _variableName + "\" удалена.";
            }
            else
                return "Переменной \"" + _variableName + "\" не существует.";
        }

        /// <summary>
        /// Ставит переменные, в выражения которых входит заданная, в состояние "inprogress" 
        /// и сбрасывает их значение для дальнейшего обновления на сервере.
        /// </summary>
        /// <param name="_variableName">Имя переменной</param>
        private void Switch_DependentVariables_InTo_InProgressStatus(string _variableName)
        {
            foreach (Variables dependentVariable in context.Variables)                                  // Ищем переменные,
            {                                                                                           // 
                List<string> list = new List<string>(dependentVariable.varExpressionRPN.Split(' '));    // в выражение которых
                foreach (string argument in list)                                                       //
                    if (argument == _variableName)                                                      // входит указанная переменная.
                    {                                                                                   //
                        dependentVariable.varStatus = "inprogress";                                     // Такие переменные ставим на пересчёт
                        dependentVariable.varValue = null;                                              // как вновь введённые.
                    }                                                                                   //
            }
            context.SaveChanges();
        }

        public string Output(string _variableName)
        {
            string name = _variableName.Replace(" ", "");
            foreach (Variables variable in context.Variables)
                if (variable.varName.Equals(name))
                {
                    if (variable.varStatus == "inprogress")
                        return "inprogress";
                    if (variable.varStatus == "correct")
                    {
                        if (variable.varValue == null)
                            return ": недостаточно данных для рассчёта.";
                        return ": " + Math.Round(Convert.ToDouble(variable.varValue), 3);
                    }
                    else
                        return variable.varStatus;
                }
            return ": с таким именем переменной не существует.";
        }

        public string Monitor()
        {
            StringBuilder result=new StringBuilder("Текущие значения переменных:\n");
            foreach (Variables variable in context.Variables.OrderBy(v => v.varName))
            {
                string value;
                if (variable.varValue == null)
                    value = "";
                else
                    value = Math.Round(Convert.ToDouble(variable.varValue), 3).ToString();
                result.AppendFormat("{0, -7} = {1, -7} = {2, -12} = {3, -12} = {4}\n", 
                    variable.varName, value, variable.varExpression, variable.varExpressionRPN, variable.varStatus);
            }
            return result.ToString();
        }
    }
}