﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Formulas_InputClient
{
    class Program
    {
        static void Main(string[] args)
        {
            var client = new Formulas.InputServiceClient("BasicHttpBinding_IInputService");

            Console.WriteLine("КЛИЕНТ ДЛЯ ВВОДА ПЕРЕМЕННЫХ");
            Console.WriteLine("===========================");
            Console.WriteLine();
            Console.WriteLine("Для переопределения ранее введённой переменной");
            Console.WriteLine("воспользуйтесь командой вида:");
            Console.WriteLine("$override: [имя_переменной] = [выражение]");
            Console.WriteLine();
            Console.WriteLine("Для удаления ранее введённой переменной");
            Console.WriteLine("воспользуйтесь командой вида:");
            Console.WriteLine("$delete: [имя_переменной]");
            Console.WriteLine();

            string equation;                                //
            while ((equation = Console.ReadLine()) != "")   // Пользователь вводит уравнение.
                Console.WriteLine(client.Input(equation));  // В ответ получает сообщение.
            
            client.Close();
        }
    }
}