﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.ServiceModel;
using Formulas_Service;

namespace Formulas_Host
{
    class Program
    {
        private static Thread calculateCycleThread;

        static void Main(string[] args)
        {
            using (ServiceHost serviceHost = new ServiceHost(typeof(Formulas_Service.MainService)))
            {

                serviceHost.Open();
                Console.CursorVisible = false;
                Console.WriteLine("СЕРВЕР ЗАПУЩЕН");

                calculateCycleThread = new Thread(CalculateCycle); // В отдельном фоновом потоке
                calculateCycleThread.Name = "CalculateCycle";      // запускаем метод циклически
                calculateCycleThread.IsBackground = true;          // обновляющий значения переменных.
                calculateCycleThread.Start();                      //

                Console.ReadKey();
            }
        }
        /// <summary>
        /// Циклически расчитывает значения переменных и сохраняет в БД
        /// </summary>
        private static void CalculateCycle()
        {
            while (true)
            {
                Formulas_localDBEntities context = new Formulas_localDBEntities();
                List<Variables> list = new List<Variables>();              // Формируем лист переменных,
                foreach (Variables variable in context.Variables)          // которые есть в базе на данный момент
                    list.Add(new Variables                                 // для его дальнейшей передачи в класс Formula.
                        {                                                  //
                            varName = variable.varName,                    //
                            varExpression = variable.varExpression,        //
                            varStatus = variable.varStatus,                //
                            varExpressionRPN = variable.varExpressionRPN,  //
                            varValue = variable.varValue                   //
                        });                                                //
                foreach (Variables variable in context.Variables)       // Перебираем переменные в Entity.DBSet<Variables>
                {                                                       //
                    Formula formula = new Formula                       //
                        {                                               //
                            Name = variable.varName,                    //
                            Expression = variable.varExpression,        //
                            Status = variable.varStatus,                //
                            ExpressionRPN = variable.varExpressionRPN,  //
                            Value = variable.varValue,                  //
                            VariablesFromDB = list                      //
                        };                                              //
                    if (formula.Status != "correct")                         // 
                        formula.Status = formula.GetStatus();                // Выявляем в уравнении наличие синтаксических ошибок.
                    if (formula.Status == "correct")                                // Если во введённом уравнении 
                    {                                                               // нет синтаксических ошибок,
                        formula.ExpressionRPN = formula.GetReversePolishNotation(); // то получаем выражение в формате ОбратПольскЗаписи.
                        if (formula.Validate())                         // Если в базе данных 
                        {                                               // достаточно данных для подсчёта выражения,
                            double? value = formula.CalculateValue();   // то получаем численное значение выражения.
                            if (value != null)                          // Если при подсчёте не выявлено ошибок,
                                formula.Value = value;                  // то сохраняем численное значение.
                        }                                               //
                    }
                    variable.varName = formula.Name;                    // Помещаем обновленные значения 
                    variable.varExpression = formula.Expression;        // обратно в Entity.DBSet<Variables>.
                    variable.varStatus = formula.Status;                //
                    variable.varExpressionRPN = formula.ExpressionRPN;  //
                    variable.varValue = formula.Value;                  //
                }
                context.SaveChanges(); // Сохраняем в базу данных.
            }
        }
    }
}