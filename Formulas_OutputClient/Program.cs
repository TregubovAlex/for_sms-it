﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Formulas_OutputClient
{
    class Program
    {
        static void Main(string[] args)
        {
            var client = new Formulas.OutputServiceClient("BasicHttpBinding_IOutputService");

            Console.WriteLine("КЛИЕНТ ДЛЯ ПОЛУЧЕНИЯ РЕЗУЛЬТАТОВ");
            Console.WriteLine("================================");
            Console.WriteLine();                            


            string variableName;                                //
            while ((variableName = Console.ReadLine()) != "")   // Пользователь вводит имя переменной.
            {
                if (client.Output(variableName) == "inprogress")     //
                {                                                    //
                    Console.Write("Ожидайте расчёта переменной");    //
                    Console.CursorVisible = false;                   //
                }                                                    //
                while (client.Output(variableName) == "inprogress")  // Пока состояние переменной "inprogress",
                {                                                    // клиент ждёт рассчёта.
                    Console.Write('.');                              //
                    System.Threading.Thread.Sleep(700);              // 
                }                                                    //
                Console.WriteLine();
                Console.SetCursorPosition(variableName.Length, Console.CursorTop - 2);
                Console.WriteLine(client.Output(variableName)); // В ответ получает значение переменной или сообщение об ошибке (Formula.Status).
                Console.CursorVisible = true;
            }
            client.Close();
        }
    }
}
