﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Formulas_MonitorClient
{
    class Program
    {
        private static Thread monitorCycleThread;
        private static Formulas.MonitorServiceClient client;

        static void Main(string[] args)
        {
            monitorCycleThread = new Thread(MonitorCycle);  // В отдельном фоновом потоке
            monitorCycleThread.Name = "MonitorCycleThread"; // запускаем метод циклически
            monitorCycleThread.IsBackground = true;         // обновляющий экран клиента-монитора.
            monitorCycleThread.Start();                     //

            Console.ReadKey();
        }
        /// <summary>
        /// Метод циклически обновляет экран клиента-монитора.
        /// </summary>
        private static void MonitorCycle()
        {
            Console.CursorVisible = false;
            using (client = new Formulas.MonitorServiceClient("BasicHttpBinding_IMonitorService"))
            {
                while (true)
                {
                    string actualData = client.Monitor();
                    Console.Clear();
                    Console.WriteLine("КЛИЕНТ-МОНИТОР");
                    Console.WriteLine("==============");
                    Console.WriteLine();
                    Console.WriteLine(actualData);
                }
                //client.Close();
            }
        }
    }
}